package br.com.rodrigopa.napp.napp;

import android.app.Application;
import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.joanzapata.iconify.Iconify;
import com.joanzapata.iconify.fonts.MaterialModule;

import br.com.rodrigopa.napp.napp.framework.database.DatabaseHelper;

public class NAppApplication extends Application {

    public static DatabaseHelper databaseHelper = null;
    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        // iconify
        Iconify.with(new MaterialModule());
        NAppApplication.context = getApplicationContext();
    }


    /**
     * You'll need this in your class to get the helper from the manager once per class.
     */
    public static DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(NAppApplication.context, DatabaseHelper.class);
        }
        return databaseHelper;
    }

}
