package br.com.rodrigopa.napp.napp.activity;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.j256.ormlite.android.apptools.OpenHelperManager;

import br.com.rodrigopa.napp.napp.NAppApplication;
import br.com.rodrigopa.napp.napp.R;
import br.com.rodrigopa.napp.napp.adapter.ViewPagerAdapter;
import br.com.rodrigopa.napp.napp.fragment.AboutFragment;
import br.com.rodrigopa.napp.napp.fragment.KeychainFragment;
import br.com.rodrigopa.napp.napp.fragment.MeditationFragment;
import br.com.rodrigopa.napp.napp.fragment.MeetFragment;
import br.com.rodrigopa.napp.napp.framework.database.DatabaseHelper;
import br.com.rodrigopa.napp.napp.notification.NotificationHelper;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener, ViewPager.OnPageChangeListener {

    /* Layout components */
    @BindView(R.id.bottom_navigation) BottomNavigationView bottomNavigationView;
    @BindView(R.id.fragment_container) ViewPager fragmentContainer;
    private ViewPagerAdapter viewPagerAdapter;
    private MenuItem prevMenuItem;
    private String[] tabTitles = { "Meditação diária", "Chaveiros", "Reuniões", "Sobre" };
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        // progress dialog
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Carregando...");

        // job manager
        NotificationHelper.enableBootReceiver(this);
        NotificationHelper.scheduleRepeatingRTCNotification(this);

        setTitle(tabTitles[0]);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);
        fragmentContainer.addOnPageChangeListener(this);
        setupViewPager();
    }

    private void setupViewPager() {
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFragment(new MeditationFragment());
        viewPagerAdapter.addFragment(new KeychainFragment());
        viewPagerAdapter.addFragment(new MeetFragment());
        viewPagerAdapter.addFragment(new AboutFragment());
        fragmentContainer.setAdapter(viewPagerAdapter);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_meditation:
                fragmentContainer.setCurrentItem(0);
                break;
            case R.id.action_keychains:
                fragmentContainer.setCurrentItem(1);
                break;
            case R.id.action_meet:
                fragmentContainer.setCurrentItem(2);
                break;
            case R.id.action_contact:
                fragmentContainer.setCurrentItem(3);
                break;
        }

        return false;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if (prevMenuItem != null) {
            prevMenuItem.setChecked(false);
        } else {
            bottomNavigationView.getMenu().getItem(0).setChecked(false);
        }

        setTitle(tabTitles[position]);
        bottomNavigationView.getMenu().getItem(position).setChecked(true);
        prevMenuItem = bottomNavigationView.getMenu().getItem(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    public void showProgressDialog() {
        progressDialog.show();
    }

    public void hideProgressDialog() {
        progressDialog.hide();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (NAppApplication.databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            NAppApplication.databaseHelper = null;
        }
    }

    /**
     * You'll need this in your class to get the helper from the manager once per class.
     */
    public DatabaseHelper getHelper() {
        return NAppApplication.getHelper();
    }
}
