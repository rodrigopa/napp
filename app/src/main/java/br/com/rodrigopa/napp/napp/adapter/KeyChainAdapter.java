package br.com.rodrigopa.napp.napp.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

import br.com.rodrigopa.napp.napp.R;
import br.com.rodrigopa.napp.napp.framework.model.KeyChain;

public class KeyChainAdapter extends RecyclerView.Adapter<KeyChainAdapter.ViewHolder> {
    public List<KeyChain> list;
    public List<String> achievementKeyChains;
    private Context context;

    public KeyChainAdapter(Context context) {
        this(new ArrayList<KeyChain>(), new ArrayList<String>(), context);
    }

    public KeyChainAdapter(List<KeyChain> list, List<String> achievementKeyChains, Context context) {
        this.list = list;
        this.achievementKeyChains = achievementKeyChains;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.keychain_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        KeyChain keyChain = list.get(position);
        holder.imageView.setImageDrawable(context.getResources().getDrawable(keyChain.resourceId));
        if (achievementKeyChains.contains(keyChain.color)) {
            Log.d("teste", "color: " + keyChain.color);
            holder.imageView.setAlpha(1f);
        } else {
            holder.imageView.setAlpha(0.1f);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;

        public ViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.keychainImage);
        }
    }
}