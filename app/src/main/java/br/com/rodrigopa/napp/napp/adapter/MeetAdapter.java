package br.com.rodrigopa.napp.napp.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.com.rodrigopa.napp.napp.R;
import br.com.rodrigopa.napp.napp.framework.model.KeyChain;
import br.com.rodrigopa.napp.napp.framework.model.Meet;
import br.com.rodrigopa.napp.napp.framework.model.TimeTable;
import br.com.rodrigopa.napp.napp.framework.model.TimeTableMeet;

public class MeetAdapter extends RecyclerView.Adapter<MeetAdapter.ViewHolder> {
    public List<Meet> list;
    private Context context;

    public MeetAdapter(Context context) {
        this.context = context;
        list = new ArrayList<>();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.meet_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Meet meet = list.get(position);
        holder.title.setText(meet.title);
        holder.address.setText(Html.fromHtml("<b>Endereço</b>: " + meet.address));
        holder.district.setText(Html.fromHtml("<b>Bairro</b>: " + meet.district));
        holder.obs.setVisibility(meet.obs.equals("") ? View.GONE : View.VISIBLE);
        holder.obs.setText(meet.obs);
        holder.timeTableRow.removeAllViews();

        LinearLayout dayLinearLayout;
        ContextThemeWrapper wrapper;
        for (TimeTable timeTable : meet.timeTableList) {
            wrapper = new ContextThemeWrapper(context, R.style.timeTableRow);
            dayLinearLayout = new LinearLayout(wrapper, null, 0);
            dayLinearLayout.setOrientation(LinearLayout.VERTICAL);
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(0,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            lp.weight = 1;
            dayLinearLayout.setLayoutParams(lp);

            for (TimeTableMeet timeTableMeet : timeTable.meetTimeList) {
                wrapper = new ContextThemeWrapper(context, R.style.timeTableRowText);
                LinearLayout.LayoutParams lpt = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT);
                lpt.bottomMargin = 4;
                TextView timeMeet = new TextView(wrapper, null, 0);
                timeMeet.setLayoutParams(lpt);
                timeMeet.setText(timeTableMeet.time);
                timeMeet.setBackgroundResource(
                        timeTableMeet.type == 0 ? R.drawable.background_meet_open :
                                R.drawable.background_meet_closed);
                dayLinearLayout.addView(timeMeet);
            }

            holder.mapButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String url = "http://maps.google.com/maps?t=m&q=" + meet.address + ", " + meet.city + ", " + meet.state;
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            });
            holder.timeTableRow.addView(dayLinearLayout, timeTable.day);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public TextView address;
        public TextView district;
        public TextView obs;
        public LinearLayout timeTableRow;
        public FloatingActionButton mapButton;

        public ViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            address = (TextView) itemView.findViewById(R.id.address);
            district = (TextView) itemView.findViewById(R.id.district);
            obs = (TextView) itemView.findViewById(R.id.obs);
            timeTableRow = (LinearLayout) itemView.findViewById(R.id.timeTableRow);
            mapButton = (FloatingActionButton) itemView.findViewById(R.id.mapButton);
        }
    }
}