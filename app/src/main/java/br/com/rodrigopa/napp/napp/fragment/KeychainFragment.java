package br.com.rodrigopa.napp.napp.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vicmikhailau.maskededittext.MaskedEditText;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.joda.time.Months;
import org.joda.time.Period;
import org.joda.time.Years;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import br.com.rodrigopa.napp.napp.R;
import br.com.rodrigopa.napp.napp.adapter.KeyChainAdapter;
import br.com.rodrigopa.napp.napp.framework.model.KeyChain;
import butterknife.BindView;
import butterknife.ButterKnife;

public class KeychainFragment extends Fragment {

    @BindView(R.id.clearDate) MaskedEditText clearDate;
    @BindView(R.id.clearRunDays) TextView clearRunDays;
    @BindView(R.id.clearDays) TextView clearDays;
    @BindView(R.id.clearMonths) TextView clearMonths;
    @BindView(R.id.clearYears) TextView clearYears;
    @BindView(R.id.recyclerView) RecyclerView recyclerView;
    @BindView(R.id.timeInfo) CardView timeInfoCardView;

    private KeyChainAdapter adapter;
    private SharedPreferences sharedPreferences;

    public static KeychainFragment newInstance() {
        return new KeychainFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_keychains, container, false);
        if (v != null) {
            ButterKnife.bind(this, v);
        }

        sharedPreferences = getActivity().getSharedPreferences(
                "br.com.rodrigopa.napp.napp", Context.MODE_PRIVATE);

        // recycler view
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        adapter = new KeyChainAdapter(getContext());
        recyclerView.setAdapter(adapter);
        clearDate.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(textView.getWindowToken(), 0);

                    // calculate clear time
                    try {
                        String clearDateString = clearDate.getText().toString();
                        if (!clearDateString.matches("([0-9]{2})/([0-9]{2})/([0-9]{4})"))
                            throw new Exception("Formato de data inválido");

                        Date date = new SimpleDateFormat("dd/MM/yyyy").parse(clearDateString);
                        sharedPreferences.edit().putString("CLEAR_DATE", clearDateString).apply();
                        updateResult(date);
                    } catch (Exception e) {
                        e.printStackTrace();
                        Snackbar.make(getActivity().findViewById(android.R.id.content),
                                "Data errada! " + clearDate.getMaskString() + " / " + e.getMessage(), Snackbar.LENGTH_LONG).show();
                    }

                    return true;
                }

                return false;
            }
        });


        String dateSaved = sharedPreferences.getString("CLEAR_DATE", null);

        if (dateSaved != null) {
            try {
                clearDate.setText(dateSaved);
                Date date = new SimpleDateFormat("dd/MM/yyyy").parse(dateSaved);
                updateResult(date);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return v;
    }

    private void updateResult(Date date) {
        LocalDate localDate = LocalDate.fromDateFields(date);

        // timeinfo
        calculateClearTime(localDate);
        timeInfoCardView.setVisibility(View.VISIBLE);

        // adapter
        adapter.list = getAllKeyChains();
        adapter.achievementKeyChains = getAchievementKeyChains(localDate);
        adapter.notifyDataSetChanged();
    }

    private List<KeyChain> getAllKeyChains() {
        List<KeyChain> list = new ArrayList<>();

        list.add(new KeyChain("white", "Ingresso", R.drawable.keychain_white));
        list.add(new KeyChain("red", "30 dias", R.drawable.keychain_red));
        list.add(new KeyChain("green", "60 dias", R.drawable.keychain_green));
        list.add(new KeyChain("wine", "90 dias", R.drawable.keychain_vine));
        list.add(new KeyChain("blue", "Seis meses", R.drawable.keychain_blue));
        list.add(new KeyChain("yellow", "Nove meses", R.drawable.keychain_yellow));
        list.add(new KeyChain("light_grey", "Um ano", R.drawable.keychain_light_grey));
        list.add(new KeyChain("dark_grey", "Dezoito meses", R.drawable.keychain_dark_grey));
        list.add(new KeyChain("black", "Múltiplos anos", R.drawable.keychain_black));

        return list;
    }

    private List<String> getAchievementKeyChains(LocalDate date) {
        List<String> colors = new ArrayList<>();

        LocalDate todayLocalDate = LocalDate.fromDateFields(new Date());
        int totalDays = Days.daysBetween(date, todayLocalDate).getDays();
        int years = Years.yearsBetween(date, todayLocalDate).getYears();
        int months = Months.monthsBetween(date, todayLocalDate).getMonths();

        colors.add("white");
        if (totalDays >= 30) colors.add("red");
        if (totalDays >= 60) colors.add("green");
        if (totalDays >= 90) colors.add("wine");
        if (months >= 6) colors.add("blue");
        if (months >= 9) colors.add("yellow");
        if (years >= 1) colors.add("light_grey");
        if (months >= 18) colors.add("dark_grey");
        if (years >= 2) colors.add("black");

        return colors;
    }

    private void calculateClearTime(LocalDate dateLocalDate) {
        Date today = new Date();

        LocalDate todayLocalDate = LocalDate.fromDateFields(today);

        int totalDays = Days.daysBetween(dateLocalDate, todayLocalDate).getDays();
        int years = Years.yearsBetween(dateLocalDate, todayLocalDate).getYears();
        int totalMonths = Months.monthsBetween(dateLocalDate, todayLocalDate).getMonths();
        int months = totalMonths % 12;
        LocalDate diffDays = todayLocalDate.minusMonths(totalMonths);
        int days =  Days.daysBetween(dateLocalDate, diffDays).getDays();

        clearRunDays.setText("" + totalDays);
        clearYears.setText("" + years + (years == 1 ? " ano" : " anos"));
        clearMonths.setText("" + months + (months == 1 ? " mes" :" meses"));
        clearDays.setText("" + days + (days == 1 ? " dia" : " dias"));
    }

}
