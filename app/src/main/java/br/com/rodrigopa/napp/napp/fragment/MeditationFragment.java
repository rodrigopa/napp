package br.com.rodrigopa.napp.napp.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.TextView;

import com.j256.ormlite.dao.Dao;

import java.text.SimpleDateFormat;
import java.util.Date;

import br.com.rodrigopa.napp.napp.R;
import br.com.rodrigopa.napp.napp.activity.MainActivity;
import br.com.rodrigopa.napp.napp.framework.meditation.MeditationScraper;
import br.com.rodrigopa.napp.napp.framework.meditation.MeditationScraperCallback;
import br.com.rodrigopa.napp.napp.framework.model.Meditation;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MeditationFragment extends Fragment {

    @BindView(R.id.meditationText)
    TextView meditationText;
    @BindView(R.id.meditationTitle)
    TextView meditationTitle;
    @BindView(R.id.meditationAction)
    TextView meditationAction;
    @BindView(R.id.meditationDescription)
    TextView meditationDescription;
    @BindView(R.id.meditationDate)
    TextView meditationDate;
    @BindView(R.id.swipeContainer)
    SwipeRefreshLayout swipeRefreshLayout;
    private MainActivity activity;
    private Meditation currentMeditation;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        activity = (MainActivity) getActivity();
        currentMeditation = getCurrentMeditation();
    }

    public static MeditationFragment newInstance() {
        return new MeditationFragment();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.fragment_meditation, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {

        switch (menuItem.getItemId()) {
            case R.id.action_share:
                String bodyTemplate = "Baixe o alicativo: {link}\nMeditação diária\n*{title}*\n{date}\n\n_{description}_\n\n{text}\n\n_*{action}*_";
                String body = bodyTemplate.replace("{date}", new SimpleDateFormat("dd/MM/yyyy").format(currentMeditation.date))
                        .replace("{title}", currentMeditation.title)
                        .replace("{description}", currentMeditation.description)
                        .replace("{text}", currentMeditation.text)
                        .replace("{action}", Html.fromHtml(currentMeditation.action).toString())
                        .replace("{link}", "https://play.google.com/store/apps/details?id=br.com.rodrigopa.napp.napp");

                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/text");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, body);
                sharingIntent.setPackage("com.whatsapp");
                startActivity(Intent.createChooser(sharingIntent, "Compartilhar"));
                return true;
        }

        return false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_metitation, container, false);
        if (v != null) {
            ButterKnife.bind(this, v);
        }

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getMeditation();
            }
        });
        Date date = new Date();

        if (currentMeditation == null) {
            getMeditation();
        } else if (!(currentMeditation.date.getDay() == date.getDay() &&
                currentMeditation.date.getMonth() == date.getMonth() &&
                currentMeditation.date.getYear() == date.getYear())) {
            getMeditation();
        }

        updateMeditationContent();

        return v;
    }

    public void getMeditation() {
        swipeRefreshLayout.setRefreshing(true);

        (new MeditationScraper(new MeditationScraperCallback() {
            @Override
            public void onSuccess(Meditation meditation) {
                // save in database
                try {
                    currentMeditation = getCurrentMeditation();

                    if (currentMeditation == null) {
                        currentMeditation = meditation;
                    } else {
                        currentMeditation.fill(meditation);
                    }

                    getDao().createOrUpdate(currentMeditation);
                    updateMeditationContent();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String error) {
            }

            public void onComplete() {
                swipeRefreshLayout.setRefreshing(false);
            }
        })).run();
    }

    private Dao<Meditation, Integer> getDao() throws Exception {
        return activity.getHelper().getMeditationDao();
    }

    private Meditation getCurrentMeditation() {
        try {
            return getDao().queryForId(1);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private void updateMeditationContent() {
        if (currentMeditation == null)
            return;

        // show
        meditationText.setText(Html.fromHtml(currentMeditation.text));
        meditationDescription.setText(currentMeditation.description);
        meditationTitle.setText(currentMeditation.title);
        meditationAction.setText(Html.fromHtml(currentMeditation.action));
        meditationDate.setText(new SimpleDateFormat("dd/MM/yyyy").format(currentMeditation.date));
    }
}
