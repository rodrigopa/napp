package br.com.rodrigopa.napp.napp.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.j256.ormlite.dao.Dao;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import br.com.rodrigopa.napp.napp.R;
import br.com.rodrigopa.napp.napp.activity.MainActivity;
import br.com.rodrigopa.napp.napp.adapter.MeetAdapter;
import br.com.rodrigopa.napp.napp.framework.meet.MeetScraper;
import br.com.rodrigopa.napp.napp.framework.meet.MeetScraperCallback;
import br.com.rodrigopa.napp.napp.framework.meet.MeetStateScraper;
import br.com.rodrigopa.napp.napp.framework.meet.MeetStateScraperCallback;
import br.com.rodrigopa.napp.napp.framework.model.Meet;
import br.com.rodrigopa.napp.napp.framework.model.MeetCity;
import br.com.rodrigopa.napp.napp.framework.model.TimeTableMeet;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MeetFragment extends Fragment {

    @BindView(R.id.spinnerCity) Spinner spinnerCity;
    @BindView(R.id.spinnerState) Spinner spinnerState;
    @BindView(R.id.meetRecyclerView) RecyclerView meetRecyclerView;
    @BindView(R.id.resultsText) TextView resultsText;
    private ArrayAdapter<String> cityAdapter;
    private ArrayAdapter<CharSequence> stateAdapter;
    private List<String> cityList;
    private List<String> stateSiglas;
    private int currentState = 0;
    private int currentCity = 0;
    private List<String> cityIdList;
    private MainActivity activity;
    private MeetAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_meet, container, false);
        if (v != null) {
            ButterKnife.bind(this, v);
        }

        activity = (MainActivity) getActivity();
        adapter = new MeetAdapter(activity);
        meetRecyclerView.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false));
        meetRecyclerView.setAdapter(adapter);

        // city
        cityList = new ArrayList<>();
        cityList.add("Cidade...");
        stateSiglas = Arrays.asList(getResources().getStringArray(R.array.estados_siglas));
        spinnerCity.setEnabled(false);
        cityAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, cityList);
        stateAdapter = ArrayAdapter.createFromResource(getActivity(), R.array.estados, android.R.layout.simple_spinner_item);
        stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerCity.setAdapter(cityAdapter);
        spinnerState.setAdapter(stateAdapter);
        spinnerState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    currentState = position;
                    getCitiesFromState();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) { }
        });
        spinnerCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    currentCity = position;
                    getMeets();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) { }
        });

        final MeetCity storedMeetCity = getCurrentMeetCity();

        if (storedMeetCity != null) {
            activity.showProgressDialog();
            new MeetScraper.JsonMeetsToObject(new MeetScraperCallback() {
                @Override
                public void onSuccess(List<Meet> meetList) {
                    updateMeetList(meetList, storedMeetCity.city, storedMeetCity.stateSigla);
                }

                @Override
                public void onError(String error) {

                }

                @Override
                public void onComplete() {
                    activity.hideProgressDialog();
                }
            }).execute(storedMeetCity.meets);
        }

        return v;
    }

    public MeetCity getCurrentMeetCity() {
        try {
            return getDao().queryForId(1);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void updateMeetList(List<Meet> meetList, String cityName, String stateName) {
        adapter.list = meetList;
        resultsText.setVisibility(View.VISIBLE);
        resultsText.setText(Html.fromHtml("Reuniões em <b>" + cityName + "/" + stateName + "</b>"));
        adapter.notifyDataSetChanged();
    }

    private void getMeets() {
        activity.showProgressDialog();
        new MeetScraper(getActivity(), this, stateSiglas.get(currentState), cityList.get(currentCity), cityIdList.get(currentCity),
                new MeetScraperCallback() {
                    @Override
                    public void onSuccess(List<Meet> meetList) {
                        updateMeetList(meetList, cityList.get(currentCity), stateSiglas.get(currentState));
                    }

                    @Override
                    public void onError(String error) {
                        Log.e("error", error);
                    }

                    @Override
                    public void onComplete() {
                        activity.hideProgressDialog();
                    }
                }).run();
    }

    public Dao<MeetCity, Integer> getDao() throws Exception {
        return activity.getHelper().getMeetCityDao();
    }

    private void getCitiesFromState() {
        activity.showProgressDialog();
        new MeetStateScraper(getActivity(), stateSiglas.get(currentState),
                new MeetStateScraperCallback() {
                    @Override
                    public void onSuccess(List<String> idList, List<String> nameList) {
                        cityIdList = idList;
                        cityList = nameList;
                        spinnerCity.setEnabled(true);
                        cityAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, cityList);
                        cityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinnerCity.setAdapter(cityAdapter);
                    }

                    @Override
                    public void onError(String error) {

                    }

                    @Override
                    public void onComplete() {
                        activity.hideProgressDialog();
                    }
        }).run();
    }

}
