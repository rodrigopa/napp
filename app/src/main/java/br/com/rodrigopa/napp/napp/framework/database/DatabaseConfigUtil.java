package br.com.rodrigopa.napp.napp.framework.database;

import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;

import static com.j256.ormlite.android.apptools.OrmLiteConfigUtil.writeConfigFile;

public class DatabaseConfigUtil extends OrmLiteConfigUtil {
    public static void main(String[] args) throws Exception {
        writeConfigFile("ormlite_config.txt");
    }
}