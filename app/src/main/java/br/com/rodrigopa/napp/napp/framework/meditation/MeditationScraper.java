package br.com.rodrigopa.napp.napp.framework.meditation;

import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.SyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.nio.charset.Charset;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import br.com.rodrigopa.napp.napp.framework.model.Meditation;
import cz.msebera.android.httpclient.Header;

public class MeditationScraper {

    private String URL = "http://na.org.br/meditacao";
    private MeditationScraperCallback callback;
    private boolean sync = false;
    private AsyncHttpResponseHandler responseHandler = new TextHttpResponseHandler() {

        @Override
        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
            if (callback != null)
                callback.onError(null);
        }

        @Override
        public void onSuccess(int statusCode, Header[] headers, String responseString) {
            try {
                Document doc = Jsoup.parse(responseString);
                Element contentText = doc.select(".content-text").first();
                Element meditationTitle = contentText.select("h2").first();
                Element meditationDescription = contentText.select(".destaque").first();
                Element meditationText = meditationDescription.nextElementSibling();
                Element meditationDateText = meditationDescription.previousElementSibling();
                Element meditationAction = contentText.select(".line h3").first();
                String dateText = meditationDateText.text().trim().split(",")[1].trim();
                Date meditationDate = new SimpleDateFormat("dd 'de' MMMMM 'de' yyyy", Locale.ENGLISH)
                        .parse(dateText);

                Meditation meditation = new Meditation(meditationTitle.text(), meditationDescription.text()
                        , meditationText.html().trim(), meditationAction.html().trim(), meditationDate);


                if (callback != null)
                    callback.onSuccess(meditation);
            } catch (ParseException e) {
                e.printStackTrace();
                if (callback != null)
                    callback.onError(null);
            }
        }

        @Override
        public void onFinish() {
            if (callback != null)
                callback.onComplete();
        }
    };

    public MeditationScraper(MeditationScraperCallback callback) {
        this.callback = callback;
        this.sync = false;
    }

    public MeditationScraper(MeditationScraperCallback callback, boolean sync) {
        this.callback = callback;
        this.sync = sync;
    }

    public void run() {
        responseHandler.setCharset("ISO-8859-1");

        if (!sync) {
            AsyncHttpClient client = new AsyncHttpClient();
            client.get(URL, responseHandler);
        } else {
            SyncHttpClient client = new SyncHttpClient();
            client.get(URL, responseHandler);
        }
    }
}
