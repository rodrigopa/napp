package br.com.rodrigopa.napp.napp.framework.meditation;

import br.com.rodrigopa.napp.napp.framework.model.Meditation;

public interface MeditationScraperCallback {

    void onSuccess(Meditation meditation);
    void onError(String error);
    void onComplete();

}
