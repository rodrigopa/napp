package br.com.rodrigopa.napp.napp.framework.meet;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.j256.ormlite.dao.Dao;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.SyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import br.com.rodrigopa.napp.napp.activity.MainActivity;
import br.com.rodrigopa.napp.napp.fragment.MeetFragment;
import br.com.rodrigopa.napp.napp.framework.meditation.MeditationScraperCallback;
import br.com.rodrigopa.napp.napp.framework.model.Meditation;
import br.com.rodrigopa.napp.napp.framework.model.Meet;
import br.com.rodrigopa.napp.napp.framework.model.MeetCity;
import br.com.rodrigopa.napp.napp.framework.model.TimeTable;
import br.com.rodrigopa.napp.napp.framework.model.TimeTableMeet;
import cz.msebera.android.httpclient.Header;

public class MeetScraper {

    private String URL = "http://na.org.br/grupo";
    private MeetScraperCallback callback;
    private Context context;
    private MeetFragment fragment;
    private String cityId;
    private String stateSigla;
    private String cityName;
    private AsyncHttpResponseHandler responseHandler = new TextHttpResponseHandler() {

        @Override
        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
            if (callback != null)
                callback.onError(null);
        }

        @Override
        public void onSuccess(int statusCode, Header[] headers, String responseString) {
            new HTMLMeetsToJson().execute(responseString);
        }

        @Override
        public void onFinish() { }
    };

    public MeetScraper(Context context, MeetFragment fragment, String stateSigla, String cityName, String cityId, MeetScraperCallback callback) {
        this.context = context;
        this.fragment = fragment;
        this.stateSigla = stateSigla;
        this.cityId = cityId;
        this.cityName = cityName;
        this.callback = callback;
    }

    public void run() {
        responseHandler.setCharset("ISO-8859-1");

        AsyncHttpClient client = new AsyncHttpClient();
        client.addHeader("Referer", "http://na.org.br/grupo");
        RequestParams requestParams = new RequestParams();
        requestParams.add("busca_grupo_estado", stateSigla);
        requestParams.add("busca_grupo_cidade", cityId);
        requestParams.add("busca_grupo_bairro", null);
        client.post(URL, requestParams, responseHandler);
    }

    private void logLarge(String veryLongString) {
        int maxLogSize = 1000;
        for(int i = 0; i <= veryLongString.length() / maxLogSize; i++) {
            int start = i * maxLogSize;
            int end = (i+1) * maxLogSize;
            end = end > veryLongString.length() ? veryLongString.length() : end;
            Log.e("teste", veryLongString.substring(start, end));
        }
    }

    private class HTMLMeetsToJson extends AsyncTask<String, Integer, Integer> {

        private JSONArray meets;

        @Override
        protected Integer doInBackground(String... strings) {
            try {
                Document doc = Jsoup.parse(strings[0]);
                Elements meetsElements = doc.select("body > div.content-home > div > div.box > div > div:nth-child(2) > div.col-md-12.no-padding > div:nth-child(4) > div:nth-child(2) > div.col-xs-12 > .line");
                meets = new JSONArray();

                JSONObject meet;
                JSONArray meetTable;
                JSONArray times;
                for (Element meetElement : meetsElements) {
                    meet = new JSONObject();
                    meet.put("title", meetElement.select("h1").first().text());
                    meet.put("address", meetElement.child(2).ownText());
                    meet.put("district", meetElement.child(3).ownText());
                    meet.put("city", meetElement.child(4).ownText());
                    meet.put("state", meetElement.child(5).ownText());
                    meet.put("cep", meetElement.child(6).ownText());

                    Element dividerIterator = meetElement.child(8);
                    StringBuilder obs = new StringBuilder();

                    while (!dividerIterator.hasClass("divider")) {
                        obs.append(dividerIterator.text());
                        obs.append("\n");
                        dividerIterator = dividerIterator.nextElementSibling();
                    }

                    meet.put("obs", obs.toString().replaceAll("\\n+$", ""));

                    // time
                    Element table = meetElement.select(".table tr").last();
                    Elements days = table.select("td");
                    meetTable = new JSONArray();
                    int dayIndex = 0;
                    for (Element day : days) {
                        Elements hours = day.select(".line");

                        times = new JSONArray();
                        for (Element dayTime : hours) {
                            times.put(
                                    new JSONObject().
                                            put("time", dayTime.ownText()).
                                            put("type", dayTime.html().contains("fechada") ? 1 : 0)
                            );

                        }

                        meetTable.put(
                                new JSONObject().
                                        put("day", dayIndex).
                                        put("times", times)
                        );

                        dayIndex++;
                    }

                    meet.put("meets", meetTable);
                    meets.put(meet);
                }

                return 1;
            } catch (Exception e) {
                e.printStackTrace();
                return 0;
            }
        }

        @Override
        protected void onPostExecute(Integer result) {
            if (result == 1) {
                try {
                    MeetCity meetCity = fragment.getCurrentMeetCity();

                    if (meetCity == null) {
                        meetCity = new MeetCity(cityName, stateSigla, meets.toString());
                    } else {
                        meetCity.fill(cityName, stateSigla, meets.toString());
                    }

                    fragment.getDao().createOrUpdate(meetCity);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                new JsonMeetsToObject(callback).execute(meets.toString());
            } else {
                if (callback != null) callback.onError("Erro");
            }
        }
    }

    public static class JsonMeetsToObject extends AsyncTask<String, Integer, Integer> {

        private List<Meet> meets;
        private MeetScraperCallback meetScraperCallback;

        public JsonMeetsToObject(MeetScraperCallback callback) {
            this.meetScraperCallback = callback;
        }

        @Override
        protected Integer doInBackground(String... strings) {
            try {
                JSONArray meetsJsonArray = new JSONArray(strings[0]);

                JSONObject meetJson;
                JSONArray meetsTimeJsonArray;
                JSONObject meetsTimeJsonObject;
                JSONArray meetsTableMeetJsonArray;
                List<TimeTableMeet> timeTableMeetList;
                List<TimeTable> timeTableList;
                meets = new ArrayList<>();

                for (int i = 0; i < meetsJsonArray.length(); i++) {
                    meetJson = meetsJsonArray.getJSONObject(i);
                    meetsTimeJsonArray = meetJson.getJSONArray("meets");
                    timeTableList = new ArrayList<>();

                    for (int m = 0; m < meetsTimeJsonArray.length(); m++) {
                        meetsTimeJsonObject = meetsTimeJsonArray.getJSONObject(m);
                        timeTableMeetList = new ArrayList<>();
                        meetsTableMeetJsonArray = meetsTimeJsonObject.getJSONArray("times");

                        for (int j = 0; j < meetsTableMeetJsonArray.length(); j++) {
                            timeTableMeetList.add(
                                    new TimeTableMeet(
                                            meetsTableMeetJsonArray.getJSONObject(j).getString("time"),
                                            meetsTableMeetJsonArray.getJSONObject(j).getInt("type")
                                    )
                            );
                        }

                        timeTableList.add(
                                new TimeTable(
                                        meetsTimeJsonArray.getJSONObject(m).getInt("day"),
                                        timeTableMeetList
                                )
                        );
                    }

                    meets.add(new Meet(
                            meetJson.getString("title"), meetJson.getString("address"),
                            meetJson.getString("district"), meetJson.getString("city"),
                            meetJson.getString("state"), meetJson.getString("cep"),
                            meetJson.getString("obs"), timeTableList
                    ));
                }

                return 1;
            } catch (Exception e) {
                e.printStackTrace();
                return 0;
            }
        }

        @Override
        protected void onPostExecute(Integer result) {
            if (meetScraperCallback != null) {
                meetScraperCallback.onComplete();

                if (result == 1)
                    meetScraperCallback.onSuccess(meets);
                else
                    meetScraperCallback.onError("Erro");
            }
        }
    }
}
