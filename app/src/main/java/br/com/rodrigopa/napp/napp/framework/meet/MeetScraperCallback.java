package br.com.rodrigopa.napp.napp.framework.meet;

import org.json.JSONArray;

import java.util.List;

import br.com.rodrigopa.napp.napp.framework.model.Meditation;
import br.com.rodrigopa.napp.napp.framework.model.Meet;

public interface MeetScraperCallback {

    void onSuccess(List<Meet> meetList);
    void onError(String error);
    void onComplete();

}
