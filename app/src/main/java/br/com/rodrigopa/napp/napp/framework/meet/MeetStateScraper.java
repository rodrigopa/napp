package br.com.rodrigopa.napp.napp.framework.meet;

import android.content.Context;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;
import com.loopj.android.http.SyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import br.com.rodrigopa.napp.napp.framework.meditation.MeditationScraperCallback;
import br.com.rodrigopa.napp.napp.framework.model.Meditation;
import cz.msebera.android.httpclient.Header;

public class MeetStateScraper {

    private String URL = "http://na.org.br/core/v1_0_3/service/json/svr_cidade_front_json.php?estado_codigo={sigla}";
    private MeetStateScraperCallback callback;
    private boolean sync = false;
    private String sigla;
    private Context context;
    private AsyncHttpResponseHandler responseHandler = new JsonHttpResponseHandler() {

        @Override
        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
            if (callback != null)
                callback.onError(null);
        }

        @Override
        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
            try {
                JSONArray cidadesArray = response.getJSONArray("Cidades");
                List<String> cidadeIdList = new ArrayList<>();
                List<String> cidadeNameList = new ArrayList<>();
                cidadeIdList.add("0");
                cidadeNameList.add("Cidade...");

                JSONObject cidadeLoop;
                for (int i = 0; i < cidadesArray.length(); i++) {
                    cidadeLoop = cidadesArray.getJSONObject(i);
                    cidadeIdList.add(cidadeLoop.getString("CidadeID"));
                    cidadeNameList.add(cidadeLoop.getString("CidadeNome").toUpperCase());
                }

                if (callback != null) {
                    callback.onSuccess(cidadeIdList, cidadeNameList);
                }

            } catch (Exception e) {
                if (callback != null)
                    callback.onError(e.getMessage());
            }
        }

        @Override
        public void onFinish() {
            if (callback != null)
                callback.onComplete();
        }
    };

    public MeetStateScraper(Context context, String sigla, MeetStateScraperCallback callback) {
        this.context = context;
        this.callback = callback;
        this.sigla = sigla;
    }

    public void run() {
        URL = URL.replace("{sigla}", sigla);
        AsyncHttpClient client = new AsyncHttpClient();
        client.addHeader("Referer", "http://na.org.br/grupo");
        client.post(URL, responseHandler);
    }
}
