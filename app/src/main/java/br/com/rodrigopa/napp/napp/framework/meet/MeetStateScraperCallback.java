package br.com.rodrigopa.napp.napp.framework.meet;

import java.util.List;

import br.com.rodrigopa.napp.napp.framework.model.Meditation;

public interface MeetStateScraperCallback {

    void onSuccess(List<String> idList, List<String> nameList);
    void onError(String error);
    void onComplete();

}
