package br.com.rodrigopa.napp.napp.framework.model;

public class KeyChain {

    public String color;
    public String description;
    public int resourceId;

    public KeyChain(String color, String description, int resourceId) {
        this.color = color;
        this.description = description;
        this.resourceId = resourceId;
    }

}
