package br.com.rodrigopa.napp.napp.framework.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

@DatabaseTable(tableName = "meditations")
public class Meditation {

    @DatabaseField(generatedId = true) Integer id;
    @DatabaseField public String title;
    @DatabaseField public String description;
    @DatabaseField public String text;
    @DatabaseField public String action;
    @DatabaseField public Date date;

    public Meditation() {}

    public Meditation(String title, String description, String text, String action, Date date) {
        this.title = title;
        this.description = description;
        this.text = text;
        this.action = action;
        this.date = date;
    }

    public void fill(Meditation meditation) {
        this.title = meditation.title;
        this.description = meditation.description;
        this.text = meditation.text;
        this.action = meditation.action;
        this.date = meditation.date;
    }

}
