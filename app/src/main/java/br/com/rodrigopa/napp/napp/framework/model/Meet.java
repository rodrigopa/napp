package br.com.rodrigopa.napp.napp.framework.model;

import java.util.List;

public class Meet {

    public String title;
    public String address;
    public String district;
    public String city;
    public String state;
    public String cep;
    public String obs;
    public List<TimeTable> timeTableList;

    public Meet(String title, String address, String district, String city, String state, String cep, String obs, List<TimeTable> timeTableList) {
        this.title = title;
        this.address = address;
        this.district = district;
        this.city = city;
        this.state = state;
        this.cep = cep;
        this.obs = obs;
        this.timeTableList = timeTableList;
    }
}
