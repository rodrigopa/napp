package br.com.rodrigopa.napp.napp.framework.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "meetcity")
public class MeetCity {

    @DatabaseField(generatedId = true) Integer id;
    @DatabaseField public String city;
    @DatabaseField public String stateSigla;
    @DatabaseField public String meets;

    public MeetCity() { }

    public MeetCity(String city, String stateSigla, String meets) {
        this.city = city;
        this.stateSigla = stateSigla;
        this.meets = meets;
    }

    public void fill(String city, String stateSigla, String meets) {
        this.city = city;
        this.stateSigla = stateSigla;
        this.meets = meets;
    }
}
