package br.com.rodrigopa.napp.napp.framework.model;

import java.util.List;

public class TimeTable {

    public int day;
    public List<TimeTableMeet> meetTimeList;

    public TimeTable(int day, List<TimeTableMeet> meetTimeList) {
        this.day = day;
        this.meetTimeList = meetTimeList;
    }

}
