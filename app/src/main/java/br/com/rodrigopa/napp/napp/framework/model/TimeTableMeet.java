package br.com.rodrigopa.napp.napp.framework.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

public class TimeTableMeet {

    public String time;
    public int type;

    public TimeTableMeet(String time, int type) {
        this.time = time;
        this.type = type;
    }

}
