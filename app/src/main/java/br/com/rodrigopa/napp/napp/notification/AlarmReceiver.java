package br.com.rodrigopa.napp.napp.notification;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.j256.ormlite.dao.Dao;

import br.com.rodrigopa.napp.napp.NAppApplication;
import br.com.rodrigopa.napp.napp.R;
import br.com.rodrigopa.napp.napp.activity.MainActivity;
import br.com.rodrigopa.napp.napp.framework.meditation.MeditationScraper;
import br.com.rodrigopa.napp.napp.framework.meditation.MeditationScraperCallback;
import br.com.rodrigopa.napp.napp.framework.model.Meditation;

public class AlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(final Context context, Intent intent) {
        (new MeditationScraper(new MeditationScraperCallback() {
            @Override
            public void onSuccess(Meditation meditation) {
                try {
                    Meditation currentMeditation = getCurrentMeditation();

                    if (currentMeditation == null) {
                        currentMeditation = meditation;
                    } else {
                        currentMeditation.fill(meditation);
                    }

                    getDao().createOrUpdate(currentMeditation);

                    // notify
                    PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
                            new Intent(context, MainActivity.class), PendingIntent.FLAG_UPDATE_CURRENT);

                    NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context, "M_CH_ID");
                    Bitmap b = BitmapFactory.decodeResource(context.getResources(), R.drawable.nasymbol_logo);
                    notificationBuilder.setAutoCancel(true)
                            .setPriority(Notification.PRIORITY_MAX)
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setLargeIcon(b)
                            .setContentIntent(contentIntent)
                            .setContentTitle("Meditação diária - " + meditation.title)
                            .setContentText(meditation.description);

                    NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                    notificationManager.notify(1, notificationBuilder.build());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String error) {
            }

            public void onComplete() {}
        }, false)).run();
    }

    private Dao<Meditation, Integer> getDao() throws Exception {
        return NAppApplication.getHelper().getMeditationDao();
    }

    private Meditation getCurrentMeditation() {
        try {
            return getDao().queryForId(1);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}